char statusUpdate(int pos) {  //pos is stopCounter

  if (pos == 0 || pos == 1) {
    return 'R';
  }

  if (pos == 2 || pos == 3) {
    return 'G';
  }

  if (pos == 4 || pos == 5) {
    return 'B';
  }
  
}
